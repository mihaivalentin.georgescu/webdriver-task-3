describe("Test suite for https://cloud.google.com/ website", () => {
    it("First test", async () => {
        await browser.navigateTo("https://cloud.google.com/");
        await browser.pause(1000);

        const search_icon = await $("//input[@placeholder='Search']");

        search_icon.click();
        await browser.pause(1000);

        search_icon.addValue("Google Cloud Platform Pricing Calculator");
        await browser.pause(1000);

        await browser.keys('Enter');
        await browser.pause(1000);

        const pricing_calculator_link = await $("//b[text()='Google Cloud Pricing Calculator']");
        pricing_calculator_link.click();
        await browser.pause(1000);

        const add_estimate = await $("//span[@class='AeBiU-kBDsod-Rtc0Jf AeBiU-kBDsod-Rtc0Jf-OWXEXe-M1Soyc']//i[text()='add']");
        add_estimate.click();
        await browser.pause(1000);

        const compute_engine = await $("//h2[text()='Compute Engine']");
        compute_engine.click();
        await browser.pause(1000);

        const add_instance_button = await $("//button[@aria-label='Increment']");
        add_instance_button.click();
        add_instance_button.click();
        add_instance_button.click();
        await browser.pause(5000);

        await browser.execute(() => {
            window.scrollTo(0, 900);
        });

        // const number_button = await $("//span[@id='c34']");
        // number_button.click();
        // // await number_button.clearValue();
        // // number_button.addValue(8);d
        // await browser.pause(1000);

        const machine_type_dropdown = await $("//div[@jsname='wSASue']//div[@class='VfPpkd-xl07Ob-XxIAqe VfPpkd-xl07Ob-XxIAqe-OWXEXe-tsQazb VfPpkd-xl07Ob VfPpkd-YPmvEd IWDrLe']");
        machine_type_dropdown.click();
        await browser.pause(1000);

        const machine_type = await $("//li[@data-value='n1-standard-8']");
        machine_type.click();
        await browser.pause(5000);

        await browser.execute(() => {
            window.scrollTo(0, 1500);
        });

        const add_gpu_button = $("//div[@class='kqQzpb P33MY']//button[@type='button']");
        add_gpu_button.click();
        await browser.pause(1000);
       
        const gpu_type_button = $("//div[@class='VfPpkd-O1htCb VfPpkd-O1htCb-OWXEXe-INsAgc VfPpkd-O1htCb-OWXEXe-SfQLQb-M1Soyc-Bz112c FkS5nd VfPpkd-O1htCb-OWXEXe-XpnDCe']//span[@class='VfPpkd-t08AT-Bz112c']");
        gpu_type_button.click();
        await browser.pause(1000);
    });
});